// Refactor the following function (refactorFunc), to make it more readable.

/**
* Description: This function takes an object as an argument, it destructures it,
* then it uses its values to check if they meet specific conditions.In the end,
* it returns a new Object, with values according to the conditions met.
*/
function refactorFunc(options) {
  const {value, value2, value3, type} = options;

  let result = {};
  
  if (type === 'Date') {
    result = value !== 0 ? {
                        lte:[String(value), value2, 'from now starting end of today'].join(' ') , 
                        gte: !value3 ? 'today' : 'tommorow'
                      } : {
                        lte: 'end of today', 
                        gte: 'today'
                      };
  } else {
    result = {lte: value};
  }
  return result;
}