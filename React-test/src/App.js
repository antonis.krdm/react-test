import React, { Component } from 'react';
import Films from "./Films"
import Heading from "./Heading";
import People from './components/people'


class App extends Component {

  state = {
    people: []
  }

  componentDidMount() {
    fetch("http://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then((data) => {
        this.setState({ people: data })
      })
      .catch(console.log)
  }

  render() {
    return (
      <div>
        <Heading />
        <Films />
        <People people={this.state.people} />
      </div>
    );
  }
}

export default App;
