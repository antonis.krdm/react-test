import React from 'react'

const People = ({ people }) => {
    return (
        <div>
            <center><h1>People List</h1></center>
            {people.map((people) => (
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{people.name}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{people.email}</h6>
                        <p class="card-text">{people.company.catchPhrase}</p>
                    </div>
                </div>
            ))}
        </div>
    )
};

export default People;