import React, { Component } from "react";

class Heading extends Component {
    render() {
        return (
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <ul class="navbar-nav">
                    <a class="navbar-brand" href="#">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Star_Wars_Logo.svg/1200px-Star_Wars_Logo.svg.png" width="200px" height="100" alt="" />
                    </a>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Films</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">People</a>
                    </li>

                </ul >
            </nav >
        );
    }
}

export default Heading;