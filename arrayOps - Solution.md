// Given the following array `[[1,2,3,4,5], [1,2,3,4,5]]`
// write a function called arrayOps, which will produce the following outcome:
// `[0,2,6,12,20,5,12,21,32,45]`

function arrayOps(arr){
  const arrCombined=arr[0].concat(arr[1]);
  const arrFinal=[];
  for(i=0;i<arrCombined.length;i++){
    arrFinal[i]=arrCombined[i]*i;
  }
    console.log(arrFinal);
}

// arrayOps(...);
