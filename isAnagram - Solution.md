// Create a function called isAnagram, which given two strings, returns true if they are anagrams of one another.
// For example: `Listen` is an anagram of `Silent`

function isAnagram(first, second) {
  if (first.length!=second.length){
      console.log('The length of the strings do not match');
      return false;
    }
  
    first = first.toLowerCase();
    second = second.toLowerCase();
  
    const arr=second.split('');
  
    for (let i of first){
            if(arr.includes(i)){
              arr.splice(arr.indexOf(i),1);
            } else {
              console.log('The strings are not an anagram');
              return false;
            }
  }
  console.log('The strings are anagrams');
  return true;
}

// isAnagram(..., ...); should return true
