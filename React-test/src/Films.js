import React, { Component } from "react";

class Films extends Component {
    render() {
        return (
            <section class="container-fluid bg-dark text-white">
                <div class="row align-items-center p-5">
                    <div class="col-lg-8">
                        <h2>Main View</h2>
                        <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?
                        </p>
                    </div>
                    <div class="col-lg-4 p-5 bg-secondary text-white">
                        <h2>Side View</h2>
                        <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit, dolorem expedita eum laborum, iusto quos quasi animi provident debitis consequuntur fugit tenetur reiciendis et maiores similique saepe ea nulla deleniti?
                        </p>
                    </div>
                </div>
            </section>

        );
    }
}

export default Films;